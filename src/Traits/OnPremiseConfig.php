<?php

declare(strict_types=1);

namespace WoellUndWoell\PimcoreHelper\Traits;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Yaml;

/**
 * Trait OnPremiseConfig
 * Typically you have config files for a bundle under ROOT/config/packages/bundle.yaml and
 * ROOT/config/packages/environment/bundle.yaml - which can be merged together. This trait enables you to have an additional
 * layer put on top of that.
 *
 * @package WoellUndWoell\PimcoreHelper\Traits
 */
trait OnPremiseConfig
{

	/** @var string */
	protected string $finaleNamespacePart = 'DependencyInjection';

	/**
	 * Merges the on-premise-config with the existing configs according to the Config object provided
	 *
	 * @param string                 $onPremiseConfigFilePath
	 * @param array                  $configArray
	 * @param ConfigurationInterface $configuration
	 *
	 * @return array
	 */
	protected function mergeConfigsWithOnPremiseConfigs(
		array $configArray,
		ConfigurationInterface $configuration,
		string $onPremiseConfigFilePath = ''
	): array {

		$bundleNameSpace = $this->getBundleNamespace();
		$bundleShortName = $this->getBundleShortName($bundleNameSpace);

		if ($onPremiseConfigFilePath === '') {

			$onPremiseConfigFilePath = PIMCORE_PRIVATE_VAR . '/bundles/'
				. $bundleNameSpace
				. '/config.yaml';

		}

		if (\file_exists($onPremiseConfigFilePath)) {

			$onPremiseConfig = Yaml::parse(\file_get_contents($onPremiseConfigFilePath));

			if (\is_array($onPremiseConfig) && isset($onPremiseConfig[$bundleShortName])) {
				$configArray[] = $onPremiseConfig[$bundleShortName];
			}

		}

		$processor = new Processor();

		return $processor->processConfiguration($configuration, $configArray);

	}


	/**
	 * @param array            $configs
	 * @param ContainerBuilder $container
	 * @param string           $containerParamName
	 *
	 * @return void
	 */
	protected function putConfigsIntoContainer(
		array $configs,
		ContainerBuilder $container,
		string $containerParamName
	): void {

		if (\substr($containerParamName, 0, 1) === '%' && \substr($containerParamName, -1, 1) === '%') {
			$containerParamName = \substr($containerParamName, 1, -1);
		}

		$container->setParameter($containerParamName, $configs);
	}


	/**
	 *
	 *
	 * @return string
	 * @throws \Exception
	 */
	private function getBundleNamespace(): string {

		$namespaces = \explode('\\', (new \ReflectionClass($this))->getNamespaceName());
		$namespaceCount = \count($namespaces);

		if (
			$namespaceCount < 2
			|| $namespaces[$namespaceCount - 1] !== $this->finaleNamespacePart
		) {
			throw new \Exception('Falscher Kontext für diese Methode/diesen Trait!');
		}

		return $namespaces[$namespaceCount - 2];

	}

	private function getBundleShortName(string $bundleNamespace): string {

		$parts = \preg_split(
			pattern: '/([[:upper:]][[:lower:]]+)/',
			subject: $bundleNamespace,
			flags: PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY
		);

		\array_pop($parts);

		return \strtolower(\implode('', $parts));

	}

}
